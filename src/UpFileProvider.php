<?php

namespace Ufile\Laravel;

use Illuminate\Support\ServiceProvider;

class UpFileProvider extends ServiceProvider
{
    public function boot()
    {
        // 复制自定义的文件到config目录
        if (!file_exists(config_path('ufile.php'))) {
            $this->publishes(array(
                __DIR__.'/config/ufile.php' => config_path('ufile.php'),
            ));
        }
    }

    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__.'/config/ufile.php', 'ufile'
        );
    }
}
